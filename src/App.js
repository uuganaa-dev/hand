import moment from "moment";
import React, { useEffect, useState } from "react";
import "react-toastify/dist/ReactToastify.css";

const App = () => {
  const [touchPositions, setTouchPositions] = useState([]);
  const [count, setCount] = useState(false);
  const [percent, setPercent] = useState(0);
  const [startdate, setstartdate] = useState(null);
  const [enddate, setEnddate] = useState(null);
  const [open, setOpen] = useState(false);
  const [black, setBlack] = useState(false);

  useEffect(() => {
    if (open) {
      setTimeout(() => {
        setBlack(true);
      }, 30000);
    }
  }, [open]);

  useEffect(() => {
    if (count) {
      if (startdate && enddate) {
        const differenceInSeconds = parseInt(enddate) - parseInt(startdate);

        if (differenceInSeconds > 0) {
          if (percent < 100) {
            setPercent((old) => old + 25);
            setCount(false);
            setstartdate(null);
            setEnddate(null);
          }
        }
      }
    }
  }, [count, enddate, startdate]);

  useEffect(() => {
    if (percent === 100) {
      setOpen(true);
    }
  }, [percent]);

  const handleTouchStart = (e) => {
    if (!count) {
      setCount(true);
      setstartdate(moment().format("mmss"));
    }
    const touches = Array.from(e.changedTouches).map((touch) => ({
      id: touch.identifier,
      x: touch.clientX,
      y: touch.clientY,
    }));
    setTouchPositions((prevPositions) => [...prevPositions, ...touches]);
  };

  const handleTouchMove = (e) => {
    const touches = Array.from(e.changedTouches).map((touch) => ({
      id: touch.identifier,
      x: touch.clientX,
      y: touch.clientY,
    }));
    setTouchPositions((prevPositions) =>
      prevPositions.map((pos) => touches.find((t) => t.id === pos.id) || pos)
    );
  };

  const handleTouchEnd = (e) => {
    const touchIds = Array.from(e.changedTouches).map(
      (touch) => touch.identifier
    );
    setTouchPositions((prevPositions) =>
      prevPositions.filter((pos) => !touchIds.includes(pos.id))
    );
    setEnddate(moment().format("mmss"));
  };

  //

  //

  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        margin: 0,
        padding: 0,
        position: "relative",
      }}
      onTouchStart={handleTouchStart}
      onTouchMove={handleTouchMove}
      onTouchEnd={handleTouchEnd}
      onTouchCancel={handleTouchEnd}
      onClick={() => {
        setTimeout(() => {
          setCount((old) => old + 25);
        }, 3000);
      }}
      className="touch-none"
    >
      <img
        src="./Untitled design.gif"
        alt="back"
        style={{
          objectFit: "cover",
          width: "100%",
          height: "100%",
          color: "white",
        }}
      />
      {percent ? (
        <div
          className={`fixed top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 text-9xl font-bold  ${
            percent === 100 ? "text-green-500" : "text-white"
          }`}
        >
          {percent}%
        </div>
      ) : (
        <div></div>
      )}
      {touchPositions.map((touch) => (
        <div
          key={touch.id}
          style={{
            position: "absolute",
            top: touch.y - 50,
            left: touch.x - 50,
            width: 100,
            height: 100,
            border: "5px solid green",
            borderRadius: "50%",
            pointerEvents: "none",
          }}
        ></div>
      ))}
      <div className="text-white fixed bottom-10 left-1/2 -translate-x-1/2 text-3xl text-center">
        Та дэлгэцэн дээр дарж хуруугаа уншуулна уу
      </div>
      <div
        className={`${
          open ? "fixed" : "hidden"
        } top-0 left-0 w-screen h-screen bg-black`}
      >
        <video
          src="./Hanover.mp4"
          autoPlay
          muted
          loop
          className="w-screen h-screen"
        ></video>
      </div>
      <div
        className={`w-screen h-screen bg-black ${
          black ? "fixed" : "hidden"
        } top-0 left-0 z-10`}
      ></div>
    </div>
  );
};

export default App;
